/// ### Esta clase contiene los datos del jugador actual // this class contains the current player information

class Player {
  late String name;
  late int points;
  late int bonus;
  late int bestBonus;

  //! Constructors

  Player.empty();

  Player({required this.name}) {
    this.points = 0;
    this.bonus = 1;
    this.bestBonus = 1;
  }

  //! Setters

  void setName(String n) {
    this.name = n;
    this.points = 0;
    this.bonus = 1;
    this.bestBonus = 1;
  }

  void sumarPuntos() {
    this.points += (10 * this.bonus);
    _aumentarBonus();
  }

  void _aumentarBonus() {
    this.bonus++;
    this._setBestBonus();
  }

  void resetRacha() => this.bonus = 1;

  void _setBestBonus() {
    if (this.bonus > this.bestBonus) this.bestBonus = this.bonus;
  }

  //! Getters

  String getName() {
    return this.name;
  }

  int getPoints() {
    return this.points;
  }

  int getBonus() {
    return this.bonus;
  }

  int getBestBonus() {
    return this.bestBonus;
  }
}
